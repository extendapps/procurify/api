/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * procurifyapi.js
 * @NApiVersion 2.x
 * @NModuleScope Public
 */
import * as https from 'N/https';
import { InitializeOptions } from './Interfaces/IInitialize';
import { CreateAccountOptions, DeleteAccountOptions, GetAccountOptions, GetAccountsOptions, UpdateAccountOptions } from './Interfaces/IAccount';
import { CreateVendorOptions, DeleteVendorOptions, GetVendorOptions, GetVendorsOptions, UpdateVendorOptions } from './Interfaces/IVendor';
import { GetPurchaseOrderOptions, GetPurchaseOrdersOptions, UpsertPOCustomFieldValueOptions } from './Interfaces/IPurchaseOrder';
import { GetItemReceiptLinesOptions } from './Interfaces/IItemReceiptLine';
import { GetContentTypesOptions } from './Interfaces/IContentType';
import { BatchUpsertObjectMapOptions, UpsertObjectMapOptions } from './Interfaces/IObjectMap';
import { Base } from './Objects/Base';
import { CreatePaymentTermOptions, DeletePaymentTermOptions, UpdatePaymentTermOptions } from './Interfaces/IPaymentTerm';
import { CreateCatalogItemOptions, DeleteCatalogItemOptions, GetCatalogItemOptions, GetCatalogItemsOptions, UpdateCatalogItemOptions } from './Interfaces/ICatalogItem';
export declare enum AccountType {
    ASSETS = 0,
    LIABILITY = 1,
    EXPENSE = 2,
    INCOME = 3,
    EQUITY = 4,
    OTHER = 5
}
export declare class ProcurifyAPI {
    private readonly domain;
    private readonly authorizationString;
    private readonly version;
    private loggingEnabled;
    constructor(domain: string, authorizationString: string, version?: string);
    set EnableLogging(toggle: boolean);
    static getAuthorizationString(username: string, password: string): string;
    static initialize(options: InitializeOptions): void;
    createPaymentTerm(options: CreatePaymentTermOptions): void;
    updatePaymentTerm(options: UpdatePaymentTermOptions): void;
    deletePaymentTerm(options: DeleteAccountOptions): void;
    createAccount(options: CreateAccountOptions): void;
    updateAccount(options: UpdateAccountOptions): void;
    deleteAccount(options: DeletePaymentTermOptions): void;
    getAccount(options: GetAccountOptions): void;
    getAccounts(options: GetAccountsOptions): void;
    createCatalogItem(options: CreateCatalogItemOptions): void;
    updateCatalogItem(options: UpdateCatalogItemOptions): void;
    deleteCatalogItem(options: DeleteCatalogItemOptions): void;
    getCatalogItem(options: GetCatalogItemOptions): void;
    getCatalogItems(options: GetCatalogItemsOptions): void;
    createVendor(options: CreateVendorOptions): void;
    updateVendor(options: UpdateVendorOptions): void;
    deleteVendor(options: DeleteVendorOptions): void;
    getVendor(options: GetVendorOptions): void;
    getVendors(options: GetVendorsOptions): void;
    getPurchaseOrder(options: GetPurchaseOrderOptions): void;
    getPurchaseOrders(options: GetPurchaseOrdersOptions): void;
    getItemReceiptLines(options: GetItemReceiptLinesOptions): void;
    getContentTypes(options: GetContentTypesOptions): void;
    upsertObjectMap(options: UpsertObjectMapOptions): void;
    batchUpsertObjectMap(options: BatchUpsertObjectMapOptions): void;
    upsertPOCustomFieldValue(options: UpsertPOCustomFieldValueOptions): void;
    private log;
    private makeDELETERequest;
    private makeGETRequest;
    private makePOSTRequest;
    private makePUTRequest;
    private makeRequest;
}
export declare type ServerCallBack = (clientResponse: https.ClientResponse) => void;
export declare type BadRequestCallBack = (base: Base) => void;

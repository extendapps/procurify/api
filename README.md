# Procurify - API

A distributable module for `Procurify` connectivity using NetSuite API's 

See https://s3-us-west-2.amazonaws.com/public-docs.procurify-staging.com/integrations/index.html#introduction for more details

## Installation Instructions

`npm install --save-dev @extendapps/procurifyapi`

Once installed, you'll need to make this module **relative** to you code in order to confirm to NetSuite's **relative** imports

### Dev-Ops Suggestions

Add the following commands to you "scripts" sectino in your package.json
> "pfyapi-setup": "cd Source; ln -s ../node_modules/\\@extendapps/procurifyapi/"

> "pfyapi-deploy": "cp Source/procurifyapi/procurifyapi.js FileCabinet/PATH_TO_ROOT/",

### Update .gitignore

Add the following to you .gitignore file

> /Source/procurifyapi/

### Create a symbolic link (Manual)

* Navigate to the root folder of you TypeScript source
* `ln -s ../node_modules/\@extendapps/procurifyapi/`

This will create a **reference** to the procurifyapi folder and allow you to use the appropriate import statement

`import {ProcurifyAPI} from './procurifyapi';`

## Usage


### More Details

[Techincal Details](docs/README.md)

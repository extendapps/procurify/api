/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseObject} from './Base';

export interface Currency extends BaseObject {
    name: string;
    rate: string;
    active: number;
    description: string;
    base: number;
    custom_fields?: any;
}

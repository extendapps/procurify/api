/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {Address} from './Address';
import {BaseObject} from './Base';

export interface Location extends BaseObject {
    name: string;
    url: string;
    logo: string;
    phone_one: string;
    fax: string;
    email: string;
    primary_billing_address: Address;
    primary_shipping_address: Address;
    language: number;
    location_timezone: string;
    active: number;
    custom_fields?: any;
}

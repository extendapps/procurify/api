/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {Account} from './Account';
import {Currency} from './Currency';
import {Department} from './Department';
import {Location} from './Location';
import {CatalogItem} from './CatalogItem';
import {Vendor} from './Vendor';
import {Address} from './Address';
import {ObjectMap} from './ObjectMap';
import {BaseObject} from './Base';

export interface PurchaseOrder extends BaseObject {
    uuid: string;
    purchase_order_no: string;
    transaction_date: string;
    due_date: string;
    total: string;
    line_items: LineItem[];
    vendor: Vendor;
    shipping_address: Address;
    shipping_method: ObjectMap;
    shipping_term: ObjectMap;
    payment_term?: ObjectMap;
    memo?: string;
    tax: string;
    discount: string;
    freight: string;
    other: string;
    created_by: User;
    custom_fields?: any;
}

export interface LineItem extends BaseObject {
    name: string;
    quantity: string;
    unit_cost: string;
    unit_type: string;
    amount: string;
    currency: Currency;
    account: Account;
    department: Department;
    location: Location;
    catalog_item: CatalogItem;
    memo: string;
    last_approver?: User;
    requester?: User;
    custom_fields?: any;
    order_description?: string;
}

export interface User extends BaseObject {
    first_name: string;
    last_name: string;
    email: string;
}

export interface POCustomField {
    purchase_order_id: number;
    custom_field_id: number;
    custom_field_value: string | number;
}

/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseObject} from './Base';

export interface Department extends BaseObject {
    name: string;
    branch_id: number;
    active: number;
    custom_fields?: any;
}

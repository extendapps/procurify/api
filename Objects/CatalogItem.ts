/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseObject} from './Base';

export interface CatalogItem extends BaseObject {
    name: string;
    unit_type: string;
    vendor: number;
    account_code: number;
    internal_sku: string;
    currency: number;
    description: string;
    product_url: string;
    cost: string;
}

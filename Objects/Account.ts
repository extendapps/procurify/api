/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseObject} from './Base';
import {AccountType} from '../procurifyapi';

export interface Account extends BaseObject {
    code: string | number;
    description?: string;
    parent?: number;
    active?: boolean;
    account_type: AccountType;
    custom_fields?: any;
}

/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseObject} from './Base';

export interface Address extends BaseObject {
    name: string;
    address_line_one: string;
    address_line_two?: string;
    city: string;
    postal_code: string;
    state_province: string;
    country: string;
    phone?: string;
    contact_name?: string;
}

/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseObject} from './Base';

export interface PaymentTerm extends BaseObject {
    name: string;
    description?: string;
    custom_fields?: any;
}

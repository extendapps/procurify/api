/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseObject} from './Base';

export interface ObjectMap extends BaseObject {
    object_id: number;
    status: 0 | 1 | 2;
    content_type: number;
    message?: string;
    name?: string;
}

/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

export interface Pagination {
    count: number;
    next?: string;
    previous?: string;
    page_size: number;
    num_pages: number;
    current_page: number;
}

export interface Metadata {
    pagination: Pagination;
}

export interface Base {
    data: any;
    metadata: Metadata;
    errors?: any;
    errors_with_codes?: ErrorsWithCode[];
}

export interface BaseObject {
    id?: number;
    external_id?: string | number;
}

export interface ErrorsWithCode {
    field: string;
    code: string;
    message: string;
}

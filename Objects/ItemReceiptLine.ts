/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseObject} from './Base';
import {PurchaseOrder} from './PurchaseOrder';

export interface ItemReceiptLine extends BaseObject {
    name: string;
    order_item_id: number; // This matches the Procurify Line ID on the PO
    purchased_quantity: string;
    received_quantity: string;
    receive_history: ReceiveLog[];
    purchase_order: PurchaseOrder;
    created_at: string; // 2019-11-05T12:18:16.097000-08:00
    updated_at: string; // 2019-11-05T12:18:16.097000-08:00
    custom_fields?: any;
}

export interface ReceiveLog {
    received_quantity: string;
    timestamp: string; // 2019-11-05T12:18:16.097000-08:00
}

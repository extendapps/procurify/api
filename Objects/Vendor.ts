/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseObject} from './Base';

export interface Vendor extends BaseObject {
    vendor_external_id?: string;
    name: string;
    active?: boolean;
    address_line_one: string;
    address_line_two?: string;
    postal_code: string;
    city: string;
    state_province: string;
    country: string;
    email: string;
    alt_email?: string;
    contact: string;
    phone: string;
    alt_phone?: string;
    fax?: string;
    comments?: string;
    url?: string;
    payment_term?: string;
    shipping_term?: string;
    custom_fields?: any;
}

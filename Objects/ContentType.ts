/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseObject} from './Base';

export interface ContentType extends BaseObject {
    app_label: ContentTypeAppLabel;
    model: ContentTypeModel;
}

export type ContentTypeModel = 'po' | 'item';

export type ContentTypeAppLabel = 'procurify' | 'receipt';

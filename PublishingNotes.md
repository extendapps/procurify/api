# How to deploy a Beta version

After we have implemented our new feature the first thing we do is to bump the version as we would also do for a regular release.

It is crucial that we add `beta.0` at the end of your version. The `.0` indicates which beta version it is. When we publish a new fix as beta, we will increment the `.0` to `.1` and so on.

So our version should, for example, look like this: `3.1.0-beta.0`.

Next we will go ahead and commit all your changes.

> In addition to the commit it is always a good practice to add a git tag to our beta version. We can add a tag with git tag `3.1.0-beta.0`
> 
> You can run `npm version 3.1.0-beta.0` to update `package.json` and create a git tag in one go (see https://docs.npmjs.com/cli/version).

Now it is time to publish our beta version.

Publishing a beta or alpha version is quite similar to publish a final version. The only difference is the `—-tag` flag in the publish command.

To publish the beta version just run the following command;

> npm publish --tag beta

# Inspect tags with NPM Info

`npm info` is a handy command that allows us to inspect the published versions and the tags of a package.

We can see that there is a stable version `3.0.0` that is associated with the latest tag.

Additionally, we have just created a new `beta` tag and linked version to `3.1.0.beta.0` it. As you can see, there is also already an alpha version, `4.0.1-alpha.0`.

Great! We have created some tags but what now?

# NPM install with tags

Consumers usually install our library by running the standard npm install command:

> npm install —-save libraryname

This command will fetch the version associated with the `latest` tag.

> Only the version labeled to latest is shown on npmjs.com.

But there’s also the possibility to install a version based on a tag. Beta users can install the brand new version with

> npm install — save libraryname@beta.

# Conclusion

Tags offer a great way to publish prereleases that can then be used by a test group.

Those prereleases offer a great way to gain experience and feedback with the new version of a library.

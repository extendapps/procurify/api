/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * procurifyapi.js
 * @NApiVersion 2.x
 * @NModuleScope Public
 */
define(["require", "exports", "N/https", "N/log", "N/encode"], function (require, exports, https, log, encode) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.ProcurifyAPI = exports.AccountType = void 0;
    var AccountType;
    (function (AccountType) {
        AccountType[AccountType["ASSETS"] = 0] = "ASSETS";
        AccountType[AccountType["LIABILITY"] = 1] = "LIABILITY";
        AccountType[AccountType["EXPENSE"] = 2] = "EXPENSE";
        AccountType[AccountType["INCOME"] = 3] = "INCOME";
        AccountType[AccountType["EQUITY"] = 4] = "EQUITY";
        AccountType[AccountType["OTHER"] = 5] = "OTHER";
    })(AccountType = exports.AccountType || (exports.AccountType = {}));
    var ProcurifyAPI = /** @class */ (function () {
        function ProcurifyAPI(domain, authorizationString, version) {
            if (version === void 0) { version = 'v3'; }
            this.domain = domain;
            this.authorizationString = authorizationString;
            this.version = version;
            this.loggingEnabled = false;
        }
        Object.defineProperty(ProcurifyAPI.prototype, "EnableLogging", {
            set: function (toggle) {
                this.loggingEnabled = toggle;
            },
            enumerable: false,
            configurable: true
        });
        ProcurifyAPI.getAuthorizationString = function (username, password) {
            return encode.convert({
                string: "".concat(username, ":").concat(password),
                inputEncoding: encode.Encoding.UTF_8,
                outputEncoding: encode.Encoding.BASE_64
            });
        };
        ProcurifyAPI.initialize = function (options) {
            try {
                var clientResponse = https.request({
                    method: https.Method.POST,
                    url: "https://".concat(options.domain, "/api/").concat(options.version, "/integrations/netsuite/initialize/"),
                    headers: {
                        'Authorization': "Basic ".concat(options.auth_string),
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    }
                });
                if (+clientResponse.code === 201) {
                    var initializeResponse = JSON.parse(clientResponse.body);
                    options.OK(initializeResponse.data.auth_string);
                }
                else {
                    // Don't use this.log here (static method)
                    log.error('initialize - failed', clientResponse);
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            }
            catch (e) {
                // Don't use this.log here (static method)
                log.error('initialize - failed', e);
                if (options.Failed) {
                    options.Failed(e);
                }
            }
        };
        ProcurifyAPI.prototype.createPaymentTerm = function (options) {
            this.log('audit', "createPaymentTerm - options", options);
            if (options.paymentTerm.external_id) {
                this.makePOSTRequest({
                    resource: 'payment-terms/',
                    payload: options.paymentTerm,
                    Created: function (clientResponse) {
                        var createPaymentTermResponse = JSON.parse(clientResponse.body);
                        options.Created(createPaymentTermResponse.data);
                    },
                    BadRequest: function (base) {
                        if (options.BadRequest) {
                            options.BadRequest(base);
                        }
                    },
                    Failed: function (clientResponse) {
                        if (options.Failed) {
                            options.Failed(clientResponse);
                        }
                    }
                });
            }
            else {
                throw 'Payment Term is missing external_id';
            }
        };
        ProcurifyAPI.prototype.updatePaymentTerm = function (options) {
            this.log('audit', "updatePaymentTerm - options", options);
            // Ensure the payload does NOT include the id field;
            delete options.paymentTerm.id;
            this.makePUTRequest({
                resource: "payment-terms/".concat(options.id, "/"),
                payload: options.paymentTerm,
                OK: function (clientResponse) {
                    var paymentTermResponse = JSON.parse(clientResponse.body);
                    options.Updated(paymentTermResponse.data);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.deletePaymentTerm = function (options) {
            this.log('audit', "deletePaymentTerm - options", options);
            this.makeDELETERequest({
                resource: "payment-terms/".concat(options.id, "/"),
                Deleted: function () {
                    options.Deleted();
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.createAccount = function (options) {
            this.log('audit', "createAccount - options", options);
            if (options.account.external_id) {
                this.makePOSTRequest({
                    resource: 'accounts/',
                    payload: options.account,
                    Created: function (clientResponse) {
                        var createAccountResponse = JSON.parse(clientResponse.body);
                        options.Created(createAccountResponse.data);
                    },
                    BadRequest: function (base) {
                        if (options.BadRequest) {
                            options.BadRequest(base);
                        }
                    },
                    Failed: function (clientResponse) {
                        if (options.Failed) {
                            options.Failed(clientResponse);
                        }
                    }
                });
            }
            else {
                throw 'Account is missing external_id';
            }
        };
        ProcurifyAPI.prototype.updateAccount = function (options) {
            this.log('audit', "updateAccount - options", options);
            // Ensure the payload does NOT include the id field;
            delete options.account.id;
            this.makePUTRequest({
                resource: "accounts/".concat(options.id, "/"),
                payload: options.account,
                OK: function (clientResponse) {
                    var accountResponse = JSON.parse(clientResponse.body);
                    options.Updated(accountResponse.data);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.deleteAccount = function (options) {
            this.log('audit', "deleteAccount - options", options);
            this.makeDELETERequest({
                resource: "accounts/".concat(options.id, "/"),
                Deleted: function () {
                    options.Deleted();
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.getAccount = function (options) {
            this.log('audit', "getAccount - options", options);
            this.makeGETRequest({
                isSingle: true,
                resource: "accounts/".concat(options.id, "/"),
                OK: function (clientResponse) {
                    var accountResponse = JSON.parse(clientResponse.body);
                    options.OK(accountResponse.data);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.getAccounts = function (options) {
            this.log('audit', "getAccounts - options", options);
            var resource = 'accounts/';
            var queryParameters = [];
            if (options.codes) {
                queryParameters.push("code=".concat(options.codes.join(',')));
            }
            if (options.status) {
                queryParameters.push("status=".concat(options.status));
            }
            if (queryParameters.length > 0) {
                resource += "?".concat(queryParameters.join('&'));
            }
            this.makeGETRequest({
                isSingle: false,
                resource: resource,
                OK: function (clientResponse) {
                    var accountsResponse = JSON.parse(clientResponse.body);
                    options.OK(accountsResponse.data);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.createCatalogItem = function (options) {
            this.log('audit', "createCatalogItem - options", options);
            if (options.catalogItem.external_id) {
                this.makePOSTRequest({
                    resource: 'catalog-items/',
                    payload: options.catalogItem,
                    Created: function (clientResponse) {
                        var catalogItemResponse = JSON.parse(clientResponse.body);
                        options.Created(catalogItemResponse.data);
                    },
                    BadRequest: function (base) {
                        if (options.BadRequest) {
                            options.BadRequest(base);
                        }
                    },
                    Failed: function (clientResponse) {
                        if (options.Failed) {
                            options.Failed(clientResponse);
                        }
                    }
                });
            }
            else {
                throw 'Vendor is missing external_id';
            }
        };
        ProcurifyAPI.prototype.updateCatalogItem = function (options) {
            this.log('audit', "updateCatalogItem - options", options);
            // Ensure the payload does NOT include the id field;
            delete options.catalogItem.id;
            this.makePUTRequest({
                resource: "catalog-items/".concat(options.id, "/"),
                payload: options.catalogItem,
                OK: function (clientResponse) {
                    var catalogItemResponse = JSON.parse(clientResponse.body);
                    options.Updated(catalogItemResponse.data);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.deleteCatalogItem = function (options) {
            this.log('audit', "deleteCatalogItem - options", options);
            this.makeDELETERequest({
                resource: "catalog-items/".concat(options.id, "/"),
                Deleted: function () {
                    options.Deleted();
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.getCatalogItem = function (options) {
            this.log('audit', "getCatalogItem - options", options);
            this.makeGETRequest({
                isSingle: true,
                resource: "catalog-items/".concat(options.id, "/"),
                OK: function (clientResponse) {
                    var catalogItemResponse = JSON.parse(clientResponse.body);
                    options.OK(catalogItemResponse.data);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.getCatalogItems = function (options) {
            this.log('audit', "getCatalogItems - options", options);
            this.makeGETRequest({
                isSingle: false,
                resource: 'catalog-items/',
                OK: function (clientResponse) {
                    var catalogItemsResponse = JSON.parse(clientResponse.body);
                    options.OK(catalogItemsResponse.data);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.createVendor = function (options) {
            this.log('audit', "createVendor - options", options);
            if (options.vendor.external_id) {
                this.makePOSTRequest({
                    resource: 'vendors/',
                    payload: options.vendor,
                    Created: function (clientResponse) {
                        var vendorResponse = JSON.parse(clientResponse.body);
                        options.Created(vendorResponse.data);
                    },
                    BadRequest: function (base) {
                        if (options.BadRequest) {
                            options.BadRequest(base);
                        }
                    },
                    Failed: function (clientResponse) {
                        if (options.Failed) {
                            options.Failed(clientResponse);
                        }
                    }
                });
            }
            else {
                throw 'Vendor is missing external_id';
            }
        };
        ProcurifyAPI.prototype.updateVendor = function (options) {
            this.log('audit', "updateVendor - options", options);
            // Ensure the payload does NOT include the id field;
            delete options.vendor.id;
            this.makePUTRequest({
                resource: "vendors/".concat(options.id, "/"),
                payload: options.vendor,
                OK: function (clientResponse) {
                    var vendorResponse = JSON.parse(clientResponse.body);
                    options.Updated(vendorResponse.data);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.deleteVendor = function (options) {
            this.log('audit', "deleteVendor - options", options);
            this.makeDELETERequest({
                resource: "vendors/".concat(options.id, "/"),
                Deleted: function () {
                    options.Deleted();
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.getVendor = function (options) {
            this.log('audit', "getVendor - options", options);
            this.makeGETRequest({
                isSingle: true,
                resource: "vendors/".concat(options.id, "/"),
                OK: function (clientResponse) {
                    var vendorResponse = JSON.parse(clientResponse.body);
                    options.OK(vendorResponse.data);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.getVendors = function (options) {
            this.log('audit', "getVendors - options", options);
            var resource = 'vendors/';
            if (options.status) {
                resource += "?status=".concat(options.status);
            }
            else if (options.ids) {
                resource += "?id=".concat(options.ids.join(','));
            }
            this.makeGETRequest({
                isSingle: false,
                resource: resource,
                OK: function (clientResponse) {
                    var vendorsResponse = JSON.parse(clientResponse.body);
                    options.OK(vendorsResponse.data);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.getPurchaseOrder = function (options) {
            this.log('audit', "getPurchaseOrder - options", options);
            this.makeGETRequest({
                isSingle: true,
                resource: "purchase-orders/".concat(options.id, "/"),
                OK: function (clientResponse) {
                    var getPurchaseOrderResponse = JSON.parse(clientResponse.body);
                    options.OK(getPurchaseOrderResponse.data);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.getPurchaseOrders = function (options) {
            this.log('audit', "getPurchaseOrders - options", options);
            var resourceFilter = 'purchase-orders/?';
            var queryParameters = [];
            var queryParameterValues = {};
            if (options.status) {
                queryParameterValues.statuses = [];
                queryParameterValues.statuses.push(options.status);
            }
            if (options.transaction_date && options.transaction_date.length > 0) {
                if (!queryParameterValues.statuses) {
                    queryParameterValues.statuses = [];
                }
                queryParameterValues.statuses.push('ignore');
                queryParameterValues.transaction_date = options.transaction_date;
            }
            if (options.ids) {
                queryParameterValues.id = "id=".concat(options.ids.join(','));
            }
            if (queryParameterValues.statuses) {
                queryParameters.push("status=".concat(queryParameterValues.statuses.join(',')));
            }
            if (queryParameterValues.transaction_date) {
                queryParameters.push("transaction_date=".concat(queryParameterValues.transaction_date));
            }
            if (queryParameterValues.id) {
                queryParameters.push("id=".concat(options.ids.join(',')));
            }
            this.makeGETRequest({
                isSingle: false,
                resource: "".concat(resourceFilter).concat(queryParameters.join('&')),
                OK: function (clientResponse) {
                    var getPurchaseOrdersResponse = JSON.parse(clientResponse.body);
                    options.OK(getPurchaseOrdersResponse.data);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.getItemReceiptLines = function (options) {
            this.log('audit', "getItemReceiptLines - options", options);
            var resource = 'item-receipt-lines/';
            if (options.status) {
                resource += "?status=".concat(options.status);
            }
            else if (options.ids) {
                resource += "?id=".concat(options.ids.join(','));
            }
            this.makeGETRequest({
                isSingle: false,
                resource: resource,
                OK: function (clientResponse) {
                    var getItemReceiptLinesResponse = JSON.parse(clientResponse.body);
                    options.OK(getItemReceiptLinesResponse.data);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.getContentTypes = function (options) {
            this.log('audit', "getContentTypes - options", options);
            var resource = 'content-types/';
            if (options.model) {
                resource += "?model=".concat(options.model);
            }
            this.makeGETRequest({
                isSingle: false,
                resource: resource,
                OK: function (clientResponse) {
                    var getContentTypesResponse = JSON.parse(clientResponse.body);
                    options.OK(getContentTypesResponse.data);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.upsertObjectMap = function (options) {
            this.log('audit', "upsertObjectMap - options", options);
            this.makePOSTRequest({
                resource: 'object-maps/',
                payload: options.objectMap,
                Created: function (clientResponse) {
                    var upsertObjectMapResponse = JSON.parse(clientResponse.body);
                    options.Created(upsertObjectMapResponse.data);
                },
                BadRequest: function (base) {
                    if (options.BadRequest) {
                        options.BadRequest(base);
                    }
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.batchUpsertObjectMap = function (options) {
            this.log('audit', "batchUpsertObjectMap - options", options);
            this.makePOSTRequest({
                resource: 'object-maps/batch/',
                payload: options.objectMaps,
                Created: function (clientResponse) {
                    var batchUpsertObjectMapResponse = JSON.parse(clientResponse.body);
                    options.Created(batchUpsertObjectMapResponse.data);
                },
                BadRequest: function (base) {
                    if (options.BadRequest) {
                        options.BadRequest(base);
                    }
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.upsertPOCustomFieldValue = function (options) {
            this.log('audit', "upsertPOCustomFieldValue - options", options);
            this.makePOSTRequest({
                resource: 'custom-fields/purchase-orders/',
                payload: options.poCustomField,
                Created: function (clientResponse) {
                    var upsertPOCustomFieldValueResponse = JSON.parse(clientResponse.body);
                    options.Created(upsertPOCustomFieldValueResponse.data);
                },
                BadRequest: function (base) {
                    if (options.BadRequest) {
                        options.BadRequest(base);
                    }
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        };
        ProcurifyAPI.prototype.log = function (level, title, details) {
            if (this.loggingEnabled) {
                log[level]({
                    title: "ProcurifyAPI: ".concat(title),
                    details: details
                });
            }
        };
        ProcurifyAPI.prototype.makeDELETERequest = function (options) {
            var makeRequestOptions = options;
            makeRequestOptions.method = https.Method.DELETE;
            makeRequestOptions.retryCount = 0;
            this.makeRequest(makeRequestOptions);
        };
        ProcurifyAPI.prototype.makeGETRequest = function (options) {
            var _this = this;
            var makeRequestConfig = {
                method: https.Method.GET,
                resource: options.resource,
                OK: function (clientResponse) {
                    if (options.isSingle) {
                        options.OK(clientResponse);
                    }
                    else {
                        var currentResponse = JSON.parse(clientResponse.body);
                        if (options.previousResponse) {
                            // Add the objects returned to the previous load and recall this endpoint
                            var previousResponse = JSON.parse(options.previousResponse.body);
                            previousResponse.data = previousResponse.data.concat(currentResponse.data);
                            options.previousResponse.body = JSON.stringify(previousResponse);
                        }
                        else {
                            // Let's make a deep copy
                            options.previousResponse = JSON.parse(JSON.stringify(clientResponse));
                        }
                        if (currentResponse.metadata && currentResponse.metadata.pagination && currentResponse.metadata.pagination.next) {
                            var paginatedRequestQuery = currentResponse.metadata.pagination.next.split('?')[1];
                            // Replace the request query with the new paginated one
                            options.resource = "".concat(options.resource.split('?')[0], "?").concat(paginatedRequestQuery);
                            _this.makeGETRequest(options);
                        }
                        else {
                            options.OK(options.previousResponse);
                        }
                    }
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                },
                retryCount: 0
            };
            this.makeRequest(makeRequestConfig);
        };
        ProcurifyAPI.prototype.makePOSTRequest = function (options) {
            var makeRequestOptions = options;
            makeRequestOptions.method = https.Method.POST;
            makeRequestOptions.retryCount = 0;
            this.makeRequest(makeRequestOptions);
        };
        ProcurifyAPI.prototype.makePUTRequest = function (options) {
            var makeRequestOptions = options;
            makeRequestOptions.method = https.Method.PUT;
            makeRequestOptions.retryCount = 0;
            this.makeRequest(makeRequestOptions);
        };
        ProcurifyAPI.prototype.makeRequest = function (options) {
            var methodName = 'makeRequest';
            this.log('audit', "".concat(methodName, " - options"), options);
            var clientResponse = null;
            var requestOptions = {
                method: options.method,
                url: "https://".concat(this.domain, "/api/").concat(this.version, "/integrations/netsuite/").concat(options.resource),
                headers: {
                    'Authorization': "Basic ".concat(this.authorizationString),
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            };
            if (options.payload) {
                // noinspection FunctionWithInconsistentReturnsJS
                requestOptions.body = JSON.stringify(options.payload, function (key, value) {
                    if (value !== '' && value !== null) {
                        return value;
                    }
                });
            }
            try {
                this.log('audit', "".concat(methodName, " - requestOptions"), requestOptions);
                clientResponse = https.request(requestOptions);
                this.log('audit', "".concat(methodName, " - clientResponse"), clientResponse);
                switch (clientResponse.code) {
                    case 200:
                        options.OK ? options.OK(clientResponse) : options.Failed(clientResponse);
                        break;
                    case 201:
                        options.Created ? options.Created(clientResponse) : options.Failed(clientResponse);
                        break;
                    case 204:
                        options.Deleted ? options.Deleted(clientResponse) : options.Failed(clientResponse);
                        break;
                    case 400:
                        options.BadRequest ? options.BadRequest(JSON.parse(clientResponse.body)) : options.Failed(clientResponse);
                        break;
                    case 401:
                        options.Unauthorized ? options.Unauthorized(clientResponse) : options.Failed(clientResponse);
                        break;
                    case 403:
                        options.Forbidden ? options.Forbidden(clientResponse) : options.Failed(clientResponse);
                        break;
                    case 404:
                        options.NotFound ? options.NotFound(clientResponse) : options.Failed(clientResponse);
                        break;
                    case 405:
                        options.MethodNotAllowed ? options.MethodNotAllowed(clientResponse) : options.Failed(clientResponse);
                        break;
                    case 406:
                        options.NotAcceptable ? options.NotAcceptable(clientResponse) : options.Failed(clientResponse);
                        break;
                    case 410:
                        options.Gone ? options.Gone(clientResponse) : options.Failed(clientResponse);
                        break;
                    case 500:
                        options.InternalServerError ? options.InternalServerError(clientResponse) : options.Failed(clientResponse);
                        break;
                    case 503:
                        options.ServiceUnavailable ? options.ServiceUnavailable(clientResponse) : options.Failed(clientResponse);
                        break;
                    default:
                        options.Failed(clientResponse);
                        break;
                }
            }
            catch (e) {
                if (options.retryCount <= 5) {
                    options.retryCount = options.retryCount + 1;
                    this.makeRequest(options);
                }
                else {
                    this.log('emergency', "".concat(methodName, " - EXCEPTION"), e);
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            }
        };
        return ProcurifyAPI;
    }());
    exports.ProcurifyAPI = ProcurifyAPI;
});

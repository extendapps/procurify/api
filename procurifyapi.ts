/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * procurifyapi.js
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

import * as https from 'N/https';
import * as log from 'N/log';
import * as encode from 'N/encode';
import {InitializeOptions, InitializeResponse} from './Interfaces/IInitialize';
import {AccountResponse, AccountsResponse, CreateAccountOptions, DeleteAccountOptions, GetAccountOptions, GetAccountsOptions, UpdateAccountOptions} from './Interfaces/IAccount';
import {CreateVendorOptions, DeleteVendorOptions, GetVendorOptions, GetVendorsOptions, UpdateVendorOptions, VendorResponse, VendorsResponse} from './Interfaces/IVendor';
import {GetPurchaseOrderOptions, GetPurchaseOrderResponse, GetPurchaseOrdersOptions, GetPurchaseOrdersResponse, UpsertPOCustomFieldValueOptions, UpsertPOCustomFieldValueResponse} from './Interfaces/IPurchaseOrder';
import {ProcurifyStatus} from './Interfaces/Base';
import {GetItemReceiptLinesOptions, GetItemReceiptLinesResponse} from './Interfaces/IItemReceiptLine';
import {GetContentTypesOptions, GetContentTypesResponse} from './Interfaces/IContentType';
import {BatchUpsertObjectMapOptions, BatchUpsertObjectMapResponse, UpsertObjectMapOptions, UpsertObjectMapResponse} from './Interfaces/IObjectMap';
import {Base} from './Objects/Base';
import {CreatePaymentTermOptions, DeletePaymentTermOptions, PaymentTermResponse, UpdatePaymentTermOptions} from './Interfaces/IPaymentTerm';
import {CatalogItemResponse, CatalogItemsResponse, CreateCatalogItemOptions, DeleteCatalogItemOptions, GetCatalogItemOptions, GetCatalogItemsOptions, UpdateCatalogItemOptions} from './Interfaces/ICatalogItem';

export enum AccountType {
    ASSETS = 0,
    LIABILITY = 1,
    EXPENSE = 2,
    INCOME = 3,
    EQUITY = 4,
    OTHER = 5
}

export class ProcurifyAPI {
    private loggingEnabled = false;

    constructor(private readonly domain: string, private readonly authorizationString: string, private readonly version: string = 'v3') {
    }

    public set EnableLogging(toggle: boolean) {
        this.loggingEnabled = toggle;
    }

    public static getAuthorizationString(username: string, password: string): string {
        return encode.convert({
            string: `${username}:${password}`,
            inputEncoding: encode.Encoding.UTF_8,
            outputEncoding: encode.Encoding.BASE_64
        });
    }

    public static initialize(options: InitializeOptions): void {
        try {
            const clientResponse: https.ClientResponse = https.request({
                method: https.Method.POST,
                url: `https://${options.domain}/api/${options.version}/integrations/netsuite/initialize/`,
                headers: {
                    'Authorization': `Basic ${options.auth_string}`,
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            });

            if (+clientResponse.code === 201) {
                const initializeResponse: InitializeResponse = JSON.parse(
                    clientResponse.body
                );
                options.OK(initializeResponse.data.auth_string);
            } else {
                // Don't use this.log here (static method)
                log.error('initialize - failed', clientResponse);
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        } catch (e) {
            // Don't use this.log here (static method)
            log.error('initialize - failed', e);
            if (options.Failed) {
                options.Failed(e);
            }
        }
    }

    public createPaymentTerm(options: CreatePaymentTermOptions): void {
        this.log('audit', `createPaymentTerm - options`, options);
        if (options.paymentTerm.external_id) {
            this.makePOSTRequest({
                resource: 'payment-terms/',
                payload: options.paymentTerm,
                Created: clientResponse => {
                    const createPaymentTermResponse: PaymentTermResponse = JSON.parse(
                        clientResponse.body
                    );
                    options.Created(createPaymentTermResponse.data);
                },
                BadRequest: base => {
                    if (options.BadRequest) {
                        options.BadRequest(base);
                    }
                },
                Failed: clientResponse => {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        } else {
            throw 'Payment Term is missing external_id';
        }
    }

    public updatePaymentTerm(options: UpdatePaymentTermOptions): void {
        this.log('audit', `updatePaymentTerm - options`, options);
        // Ensure the payload does NOT include the id field;
        delete options.paymentTerm.id;

        this.makePUTRequest({
            resource: `payment-terms/${options.id}/`,
            payload: options.paymentTerm,
            OK: clientResponse => {
                const paymentTermResponse: PaymentTermResponse = JSON.parse(
                    clientResponse.body
                );

                options.Updated(paymentTermResponse.data);
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public deletePaymentTerm(options: DeleteAccountOptions): void {
        this.log('audit', `deletePaymentTerm - options`, options);
        this.makeDELETERequest({
            resource: `payment-terms/${options.id}/`,
            Deleted: () => {
                options.Deleted();
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public createAccount(options: CreateAccountOptions): void {
        this.log('audit', `createAccount - options`, options);
        if (options.account.external_id) {
            this.makePOSTRequest({
                resource: 'accounts/',
                payload: options.account,
                Created: clientResponse => {
                    const createAccountResponse: AccountResponse = JSON.parse(
                        clientResponse.body
                    );

                    options.Created(createAccountResponse.data);
                },
                BadRequest: base => {
                    if (options.BadRequest) {
                        options.BadRequest(base);
                    }
                },
                Failed: clientResponse => {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        } else {
            throw 'Account is missing external_id';
        }
    }

    public updateAccount(options: UpdateAccountOptions): void {
        this.log('audit', `updateAccount - options`, options);
        // Ensure the payload does NOT include the id field;
        delete options.account.id;

        this.makePUTRequest({
            resource: `accounts/${options.id}/`,
            payload: options.account,
            OK: clientResponse => {
                const accountResponse: AccountResponse = JSON.parse(
                    clientResponse.body
                );

                options.Updated(accountResponse.data);
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public deleteAccount(options: DeletePaymentTermOptions): void {
        this.log('audit', `deleteAccount - options`, options);
        this.makeDELETERequest({
            resource: `accounts/${options.id}/`,
            Deleted: () => {
                options.Deleted();
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public getAccount(options: GetAccountOptions): void {
        this.log('audit', `getAccount - options`, options);
        this.makeGETRequest({
            isSingle: true,
            resource: `accounts/${options.id}/`,
            OK: clientResponse => {
                const accountResponse: AccountResponse = JSON.parse(
                    clientResponse.body
                );
                options.OK(accountResponse.data);
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public getAccounts(options: GetAccountsOptions): void {
        this.log('audit', `getAccounts - options`, options);
        let resource = 'accounts/';
        const queryParameters: string[] = [];
        if (options.codes) {
            queryParameters.push(`code=${options.codes.join(',')}`);
        }
        if (options.status) {
            queryParameters.push(`status=${options.status}`);
        }

        if (queryParameters.length > 0) {
            resource += `?${queryParameters.join('&')}`;
        }

        this.makeGETRequest({
            isSingle: false,
            resource: resource,
            OK: clientResponse => {
                const accountsResponse: AccountsResponse = JSON.parse(
                    clientResponse.body
                );
                options.OK(accountsResponse.data);
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public createCatalogItem(options: CreateCatalogItemOptions): void {
        this.log('audit', `createCatalogItem - options`, options);
        if (options.catalogItem.external_id) {
            this.makePOSTRequest({
                resource: 'catalog-items/',
                payload: options.catalogItem,
                Created: clientResponse => {
                    const catalogItemResponse: CatalogItemResponse = JSON.parse(
                        clientResponse.body
                    );
                    options.Created(catalogItemResponse.data);
                },
                BadRequest: base => {
                    if (options.BadRequest) {
                        options.BadRequest(base);
                    }
                },
                Failed: clientResponse => {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        } else {
            throw 'Vendor is missing external_id';
        }
    }

    public updateCatalogItem(options: UpdateCatalogItemOptions): void {
        this.log('audit', `updateCatalogItem - options`, options);
        // Ensure the payload does NOT include the id field;
        delete options.catalogItem.id;

        this.makePUTRequest({
            resource: `catalog-items/${options.id}/`,
            payload: options.catalogItem,
            OK: clientResponse => {
                const catalogItemResponse: CatalogItemResponse = JSON.parse(
                    clientResponse.body
                );
                options.Updated(catalogItemResponse.data);
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public deleteCatalogItem(options: DeleteCatalogItemOptions): void {
        this.log('audit', `deleteCatalogItem - options`, options);
        this.makeDELETERequest({
            resource: `catalog-items/${options.id}/`,
            Deleted: () => {
                options.Deleted();
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public getCatalogItem(options: GetCatalogItemOptions): void {
        this.log('audit', `getCatalogItem - options`, options);
        this.makeGETRequest({
            isSingle: true,
            resource: `catalog-items/${options.id}/`,
            OK: clientResponse => {
                const catalogItemResponse: CatalogItemResponse = JSON.parse(
                    clientResponse.body
                );
                options.OK(catalogItemResponse.data);
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public getCatalogItems(options: GetCatalogItemsOptions): void {
        this.log('audit', `getCatalogItems - options`, options);

        this.makeGETRequest({
            isSingle: false,
            resource: 'catalog-items/',
            OK: clientResponse => {
                const catalogItemsResponse: CatalogItemsResponse = JSON.parse(
                    clientResponse.body
                );
                options.OK(catalogItemsResponse.data);
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public createVendor(options: CreateVendorOptions): void {
        this.log('audit', `createVendor - options`, options);
        if (options.vendor.external_id) {
            this.makePOSTRequest({
                resource: 'vendors/',
                payload: options.vendor,
                Created: clientResponse => {
                    const vendorResponse: VendorResponse = JSON.parse(
                        clientResponse.body
                    );
                    options.Created(vendorResponse.data);
                },
                BadRequest: base => {
                    if (options.BadRequest) {
                        options.BadRequest(base);
                    }
                },
                Failed: clientResponse => {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                }
            });
        } else {
            throw 'Vendor is missing external_id';
        }
    }

    public updateVendor(options: UpdateVendorOptions): void {
        this.log('audit', `updateVendor - options`, options);
        // Ensure the payload does NOT include the id field;
        delete options.vendor.id;

        this.makePUTRequest({
            resource: `vendors/${options.id}/`,
            payload: options.vendor,
            OK: clientResponse => {

                const vendorResponse: VendorResponse = JSON.parse(
                    clientResponse.body
                );
                options.Updated(vendorResponse.data);
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public deleteVendor(options: DeleteVendorOptions): void {
        this.log('audit', `deleteVendor - options`, options);
        this.makeDELETERequest({
            resource: `vendors/${options.id}/`,
            Deleted: () => {
                options.Deleted();
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public getVendor(options: GetVendorOptions): void {
        this.log('audit', `getVendor - options`, options);
        this.makeGETRequest({
            isSingle: true,
            resource: `vendors/${options.id}/`,
            OK: clientResponse => {
                const vendorResponse: VendorResponse = JSON.parse(
                    clientResponse.body
                );
                options.OK(vendorResponse.data);
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public getVendors(options: GetVendorsOptions): void {
        this.log('audit', `getVendors - options`, options);
        let resource = 'vendors/';
        if (options.status) {
            resource += `?status=${options.status}`;
        } else if (options.ids) {
            resource += `?id=${options.ids.join(',')}`;
        }
        this.makeGETRequest({
            isSingle: false,
            resource: resource,
            OK: clientResponse => {
                const vendorsResponse: VendorsResponse = JSON.parse(
                    clientResponse.body
                );
                options.OK(vendorsResponse.data);
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public getPurchaseOrder(options: GetPurchaseOrderOptions): void {
        this.log('audit', `getPurchaseOrder - options`, options);
        this.makeGETRequest({
            isSingle: true,
            resource: `purchase-orders/${options.id}/`,
            OK: clientResponse => {
                const getPurchaseOrderResponse: GetPurchaseOrderResponse = JSON.parse(
                    clientResponse.body
                );
                options.OK(getPurchaseOrderResponse.data);
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public getPurchaseOrders(options: GetPurchaseOrdersOptions): void {
        this.log('audit', `getPurchaseOrders - options`, options);
        const resourceFilter = 'purchase-orders/?';
        const queryParameters: string[] = [];
        const queryParameterValues: { statuses?: ProcurifyStatus[], id?: string, transaction_date?: string } = {};

        if (options.status) {
            queryParameterValues.statuses = [];
            queryParameterValues.statuses.push(options.status);
        }

        if (options.transaction_date && options.transaction_date.length > 0) {
            if (!queryParameterValues.statuses) {
                queryParameterValues.statuses = [];
            }
            queryParameterValues.statuses.push('ignore');
            queryParameterValues.transaction_date = options.transaction_date;
        }

        if (options.ids) {
            queryParameterValues.id = `id=${options.ids.join(',')}`;
        }

        if (queryParameterValues.statuses) {
            queryParameters.push(`status=${queryParameterValues.statuses.join(',')}`);
        }

        if (queryParameterValues.transaction_date) {
            queryParameters.push(`transaction_date=${queryParameterValues.transaction_date}`);
        }

        if (queryParameterValues.id) {
            queryParameters.push(`id=${options.ids.join(',')}`);
        }

        this.makeGETRequest({
            isSingle: false,
            resource: `${resourceFilter}${queryParameters.join('&')}`,
            OK: clientResponse => {
                const getPurchaseOrdersResponse: GetPurchaseOrdersResponse = JSON.parse(
                    clientResponse.body
                );

                options.OK(getPurchaseOrdersResponse.data);
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public getItemReceiptLines(options: GetItemReceiptLinesOptions): void {
        this.log('audit', `getItemReceiptLines - options`, options);

        let resource = 'item-receipt-lines/';
        if (options.status) {
            resource += `?status=${options.status}`;
        } else if (options.ids) {
            resource += `?id=${options.ids.join(',')}`;
        }
        this.makeGETRequest({
            isSingle: false,
            resource: resource,
            OK: clientResponse => {
                const getItemReceiptLinesResponse: GetItemReceiptLinesResponse = JSON.parse(
                    clientResponse.body
                );

                options.OK(getItemReceiptLinesResponse.data);
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public getContentTypes(options: GetContentTypesOptions): void {
        this.log('audit', `getContentTypes - options`, options);
        let resource = 'content-types/';
        if (options.model) {
            resource += `?model=${options.model}`;
        }
        this.makeGETRequest({
            isSingle: false,
            resource: resource,
            OK: clientResponse => {
                const getContentTypesResponse: GetContentTypesResponse = JSON.parse(
                    clientResponse.body
                );
                options.OK(getContentTypesResponse.data);
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public upsertObjectMap(options: UpsertObjectMapOptions): void {
        this.log('audit', `upsertObjectMap - options`, options);

        this.makePOSTRequest({
            resource: 'object-maps/',
            payload: options.objectMap,
            Created: clientResponse => {
                const upsertObjectMapResponse: UpsertObjectMapResponse = JSON.parse(
                    clientResponse.body
                );
                options.Created(upsertObjectMapResponse.data);
            },
            BadRequest: base => {
                if (options.BadRequest) {
                    options.BadRequest(base);
                }
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public batchUpsertObjectMap(options: BatchUpsertObjectMapOptions): void {
        this.log('audit', `batchUpsertObjectMap - options`, options);
        this.makePOSTRequest({
            resource: 'object-maps/batch/',
            payload: options.objectMaps,
            Created: clientResponse => {
                const batchUpsertObjectMapResponse: BatchUpsertObjectMapResponse = JSON.parse(
                    clientResponse.body
                );
                options.Created(batchUpsertObjectMapResponse.data);
            },
            BadRequest: base => {
                if (options.BadRequest) {
                    options.BadRequest(base);
                }
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    public upsertPOCustomFieldValue(options: UpsertPOCustomFieldValueOptions): void {
        this.log('audit', `upsertPOCustomFieldValue - options`, options);
        this.makePOSTRequest({
            resource: 'custom-fields/purchase-orders/',
            payload: options.poCustomField,
            Created: clientResponse => {
                const upsertPOCustomFieldValueResponse: UpsertPOCustomFieldValueResponse = JSON.parse(
                    clientResponse.body
                );
                options.Created(upsertPOCustomFieldValueResponse.data);
            },
            BadRequest: base => {
                if (options.BadRequest) {
                    options.BadRequest(base);
                }
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        });
    }

    private log(level: 'audit' | 'error' | 'emergency' | 'debug', title: string, details: unknown) {
        if (this.loggingEnabled) {
            log[level]({
                title: `ProcurifyAPI: ${title}`,
                details: details
            });
        }
    }

    private makeDELETERequest(options: MakeDELETERequestOptions): void {
        const makeRequestOptions: MakeRequestOptions = <never>options;
        makeRequestOptions.method = https.Method.DELETE;
        makeRequestOptions.retryCount = 0;
        this.makeRequest(makeRequestOptions);
    }

    private makeGETRequest(options: MakeGETRequestOptions): void {
        const makeRequestConfig: MakeRequestOptions = {
            method: https.Method.GET,
            resource: options.resource,
            OK: clientResponse => {
                if (options.isSingle) {
                    options.OK(clientResponse);
                } else {
                    const currentResponse: Base = JSON.parse(clientResponse.body);

                    if (options.previousResponse) {
                        // Add the objects returned to the previous load and recall this endpoint
                        const previousResponse: Base = JSON.parse(options.previousResponse.body);
                        previousResponse.data = previousResponse.data.concat(currentResponse.data);
                        options.previousResponse.body = JSON.stringify(previousResponse);
                    } else {
                        // Let's make a deep copy
                        options.previousResponse = JSON.parse(JSON.stringify(clientResponse));
                    }

                    if (currentResponse.metadata && currentResponse.metadata.pagination && currentResponse.metadata.pagination.next) {
                        const paginatedRequestQuery: string = currentResponse.metadata.pagination.next.split('?')[1];
                        // Replace the request query with the new paginated one
                        options.resource = `${options.resource.split('?')[0]}?${paginatedRequestQuery}`;
                        this.makeGETRequest(options);
                    } else {
                        options.OK(options.previousResponse);
                    }
                }
            },
            Failed: clientResponse => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            },
            retryCount: 0
        };

        this.makeRequest(makeRequestConfig);
    }

    private makePOSTRequest(options: MakePOSTRequestOptions): void {
        const makeRequestOptions: MakeRequestOptions = <MakeRequestOptions>options;
        makeRequestOptions.method = https.Method.POST;
        makeRequestOptions.retryCount = 0;
        this.makeRequest(makeRequestOptions);
    }

    private makePUTRequest(options: MakePUTRequestOptions): void {
        const makeRequestOptions: MakeRequestOptions = <MakeRequestOptions>options;
        makeRequestOptions.method = https.Method.PUT;
        makeRequestOptions.retryCount = 0;
        this.makeRequest(makeRequestOptions);
    }

    private makeRequest(options: MakeRequestOptions): void {
        const methodName = 'makeRequest';
        this.log('audit', `${methodName} - options`, options);
        let clientResponse: https.ClientResponse = null;

        const requestOptions: https.RequestOptions = {
            method: options.method,
            url: `https://${this.domain}/api/${this.version}/integrations/netsuite/${options.resource}`,
            headers: {
                'Authorization': `Basic ${this.authorizationString}`,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        };

        if (options.payload) {
            // noinspection FunctionWithInconsistentReturnsJS
            requestOptions.body = JSON.stringify(options.payload, (key, value) => {
                if (value !== '' && value !== null) {
                    return value;
                }
            });
        }

        try {
            this.log('audit', `${methodName} - requestOptions`, requestOptions);
            clientResponse = https.request(requestOptions);
            this.log('audit', `${methodName} - clientResponse`, clientResponse);

            switch (clientResponse.code) {
                case 200:
                    options.OK ? options.OK(clientResponse) : options.Failed(clientResponse);
                    break;

                case 201:
                    options.Created ? options.Created(clientResponse) : options.Failed(clientResponse);
                    break;

                case 204:
                    options.Deleted ? options.Deleted(clientResponse) : options.Failed(clientResponse);
                    break;

                case 400:
                    options.BadRequest ? options.BadRequest(JSON.parse(clientResponse.body)) : options.Failed(clientResponse);
                    break;

                case 401:
                    options.Unauthorized ? options.Unauthorized(clientResponse) : options.Failed(clientResponse);
                    break;

                case 403:
                    options.Forbidden ? options.Forbidden(clientResponse) : options.Failed(clientResponse);
                    break;

                case 404:
                    options.NotFound ? options.NotFound(clientResponse) : options.Failed(clientResponse);
                    break;

                case 405:
                    options.MethodNotAllowed ? options.MethodNotAllowed(clientResponse) : options.Failed(clientResponse);
                    break;

                case 406:
                    options.NotAcceptable ? options.NotAcceptable(clientResponse) : options.Failed(clientResponse);
                    break;

                case 410:
                    options.Gone ? options.Gone(clientResponse) : options.Failed(clientResponse);
                    break;

                case 500:
                    options.InternalServerError ? options.InternalServerError(clientResponse) : options.Failed(clientResponse);
                    break;

                case 503:
                    options.ServiceUnavailable ? options.ServiceUnavailable(clientResponse) : options.Failed(clientResponse);
                    break;

                default:
                    options.Failed(clientResponse);
                    break;
            }
        } catch (e) {
            if (options.retryCount <= 5) {
                options.retryCount = options.retryCount + 1;
                this.makeRequest(options);
            } else {
                this.log('emergency', `${methodName} - EXCEPTION`, e);
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            }
        }
    }
}

interface BaseRequestOptions {
    resource: string;
    Failed: ServerCallBack;
}

interface MakeGETRequestOptions extends BaseRequestOptions {
    isSingle: boolean;
    previousResponse?: https.ClientResponse;
    OK: ServerCallBack;
}

interface MakePOSTRequestOptions extends BaseRequestOptions {
    payload: unknown;
    Created: ServerCallBack;
    BadRequest: BadRequestCallBack;
}

interface MakeDELETERequestOptions extends BaseRequestOptions {
    Deleted(): void;
}

interface MakePUTRequestOptions extends BaseRequestOptions {
    payload: unknown;
    OK: ServerCallBack;
}

interface MakeRequestOptions extends BaseRequestOptions {
    method: https.Method;
    payload?: unknown;
    OK?: ServerCallBack;
    Deleted?: ServerCallBack;
    Created?: ServerCallBack;
    BadRequest?: BadRequestCallBack;
    Unauthorized?: ServerCallBack;
    Forbidden?: ServerCallBack;
    NotFound?: ServerCallBack;
    MethodNotAllowed?: ServerCallBack;
    NotAcceptable?: ServerCallBack;
    Gone?: ServerCallBack;
    InternalServerError?: ServerCallBack;
    ServiceUnavailable?: ServerCallBack;
    retryCount: number;
}

export type ServerCallBack = (clientResponse: https.ClientResponse) => void;
export type BadRequestCallBack = (base: Base) => void;

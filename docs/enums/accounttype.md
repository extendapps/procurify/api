[@extendapps/procurifyapi](../README.md) / AccountType

# Enumeration: AccountType

## Table of contents

### Enumeration members

- [ASSETS](AccountType.md#assets)
- [EQUITY](AccountType.md#equity)
- [EXPENSE](AccountType.md#expense)
- [INCOME](AccountType.md#income)
- [LIABILITY](AccountType.md#liability)
- [OTHER](AccountType.md#other)

## Enumeration members

### ASSETS

• **ASSETS** = `0`

___

### EQUITY

• **EQUITY** = `4`

___

### EXPENSE

• **EXPENSE** = `2`

___

### INCOME

• **INCOME** = `3`

___

### LIABILITY

• **LIABILITY** = `1`

___

### OTHER

• **OTHER** = `5`

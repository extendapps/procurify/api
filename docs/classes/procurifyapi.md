[@extendapps/procurifyapi](../README.md) / ProcurifyAPI

# Class: ProcurifyAPI

## Table of contents

### Constructors

- [constructor](ProcurifyAPI.md#constructor)

### Accessors

- [EnableLogging](ProcurifyAPI.md#enablelogging)

### Methods

- [batchUpsertObjectMap](ProcurifyAPI.md#batchupsertobjectmap)
- [createAccount](ProcurifyAPI.md#createaccount)
- [createCatalogItem](ProcurifyAPI.md#createcatalogitem)
- [createPaymentTerm](ProcurifyAPI.md#createpaymentterm)
- [createVendor](ProcurifyAPI.md#createvendor)
- [deleteAccount](ProcurifyAPI.md#deleteaccount)
- [deleteCatalogItem](ProcurifyAPI.md#deletecatalogitem)
- [deletePaymentTerm](ProcurifyAPI.md#deletepaymentterm)
- [deleteVendor](ProcurifyAPI.md#deletevendor)
- [getAccount](ProcurifyAPI.md#getaccount)
- [getAccounts](ProcurifyAPI.md#getaccounts)
- [getCatalogItem](ProcurifyAPI.md#getcatalogitem)
- [getCatalogItems](ProcurifyAPI.md#getcatalogitems)
- [getContentTypes](ProcurifyAPI.md#getcontenttypes)
- [getItemReceiptLines](ProcurifyAPI.md#getitemreceiptlines)
- [getPurchaseOrder](ProcurifyAPI.md#getpurchaseorder)
- [getPurchaseOrders](ProcurifyAPI.md#getpurchaseorders)
- [getVendor](ProcurifyAPI.md#getvendor)
- [getVendors](ProcurifyAPI.md#getvendors)
- [updateAccount](ProcurifyAPI.md#updateaccount)
- [updateCatalogItem](ProcurifyAPI.md#updatecatalogitem)
- [updatePaymentTerm](ProcurifyAPI.md#updatepaymentterm)
- [updateVendor](ProcurifyAPI.md#updatevendor)
- [upsertObjectMap](ProcurifyAPI.md#upsertobjectmap)
- [upsertPOCustomFieldValue](ProcurifyAPI.md#upsertpocustomfieldvalue)
- [getAuthorizationString](ProcurifyAPI.md#getauthorizationstring)
- [initialize](ProcurifyAPI.md#initialize)

## Constructors

### constructor

• **new ProcurifyAPI**(`domain`, `authorizationString`, `version?`)

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `domain` | `string` | `undefined` |
| `authorizationString` | `string` | `undefined` |
| `version` | `string` | `'v3'` |

## Accessors

### EnableLogging

• `set` **EnableLogging**(`toggle`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `toggle` | `boolean` |

#### Returns

`void`

## Methods

### batchUpsertObjectMap

▸ **batchUpsertObjectMap**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `BatchUpsertObjectMapOptions` |

#### Returns

`void`

___

### createAccount

▸ **createAccount**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `CreateAccountOptions` |

#### Returns

`void`

___

### createCatalogItem

▸ **createCatalogItem**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `CreateCatalogItemOptions` |

#### Returns

`void`

___

### createPaymentTerm

▸ **createPaymentTerm**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `CreatePaymentTermOptions` |

#### Returns

`void`

___

### createVendor

▸ **createVendor**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `CreateVendorOptions` |

#### Returns

`void`

___

### deleteAccount

▸ **deleteAccount**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `DeletePaymentTermOptions` |

#### Returns

`void`

___

### deleteCatalogItem

▸ **deleteCatalogItem**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `DeleteCatalogItemOptions` |

#### Returns

`void`

___

### deletePaymentTerm

▸ **deletePaymentTerm**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `DeleteAccountOptions` |

#### Returns

`void`

___

### deleteVendor

▸ **deleteVendor**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `DeleteVendorOptions` |

#### Returns

`void`

___

### getAccount

▸ **getAccount**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `GetAccountOptions` |

#### Returns

`void`

___

### getAccounts

▸ **getAccounts**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `GetAccountsOptions` |

#### Returns

`void`

___

### getCatalogItem

▸ **getCatalogItem**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `GetCatalogItemOptions` |

#### Returns

`void`

___

### getCatalogItems

▸ **getCatalogItems**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `GetCatalogItemsOptions` |

#### Returns

`void`

___

### getContentTypes

▸ **getContentTypes**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `GetContentTypesOptions` |

#### Returns

`void`

___

### getItemReceiptLines

▸ **getItemReceiptLines**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `GetItemReceiptLinesOptions` |

#### Returns

`void`

___

### getPurchaseOrder

▸ **getPurchaseOrder**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `GetPurchaseOrderOptions` |

#### Returns

`void`

___

### getPurchaseOrders

▸ **getPurchaseOrders**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `GetPurchaseOrdersOptions` |

#### Returns

`void`

___

### getVendor

▸ **getVendor**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `GetVendorOptions` |

#### Returns

`void`

___

### getVendors

▸ **getVendors**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `GetVendorsOptions` |

#### Returns

`void`

___

### updateAccount

▸ **updateAccount**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `UpdateAccountOptions` |

#### Returns

`void`

___

### updateCatalogItem

▸ **updateCatalogItem**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `UpdateCatalogItemOptions` |

#### Returns

`void`

___

### updatePaymentTerm

▸ **updatePaymentTerm**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `UpdatePaymentTermOptions` |

#### Returns

`void`

___

### updateVendor

▸ **updateVendor**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `UpdateVendorOptions` |

#### Returns

`void`

___

### upsertObjectMap

▸ **upsertObjectMap**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `UpsertObjectMapOptions` |

#### Returns

`void`

___

### upsertPOCustomFieldValue

▸ **upsertPOCustomFieldValue**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `UpsertPOCustomFieldValueOptions` |

#### Returns

`void`

___

### getAuthorizationString

▸ `Static` **getAuthorizationString**(`username`, `password`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `username` | `string` |
| `password` | `string` |

#### Returns

`string`

___

### initialize

▸ `Static` **initialize**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | `InitializeOptions` |

#### Returns

`void`

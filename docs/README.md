@extendapps/procurifyapi

# @extendapps/procurifyapi

## Table of contents

### Enumerations

- [AccountType](enums/AccountType.md)

### Classes

- [ProcurifyAPI](classes/ProcurifyAPI.md)

### Type aliases

- [BadRequestCallBack](README.md#badrequestcallback)
- [ServerCallBack](README.md#servercallback)

## Type aliases

### BadRequestCallBack

Ƭ **BadRequestCallBack**: (`base`: `Base`) => `void`

#### Type declaration

▸ (`base`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `base` | `Base` |

##### Returns

`void`

___

### ServerCallBack

Ƭ **ServerCallBack**: (`clientResponse`: `https.ClientResponse`) => `void`

#### Type declaration

▸ (`clientResponse`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `https.ClientResponse` |

##### Returns

`void`

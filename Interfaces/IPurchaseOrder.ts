/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {Base} from '../Objects/Base';
import {POCustomField, PurchaseOrder} from '../Objects/PurchaseOrder';
import {BaseOptions, BaseStatusOptions} from './Base';

export interface GetPurchaseOrdersOptions extends BaseStatusOptions {
    transaction_date?: string;
    OK: GetPurchaseOrdersCallback;
}

export interface GetPurchaseOrderOptions extends BaseOptions {
    id: number;
    OK: GetPurchaseOrderCallback;

    NotFound(): void;
}

export type GetPurchaseOrdersCallback = (purchaseOrders: PurchaseOrder[]) => void;

export type GetPurchaseOrderCallback = (purchaseOrder: PurchaseOrder) => void;

export interface GetPurchaseOrdersResponse extends Base {
    data: PurchaseOrder[];
}

export interface GetPurchaseOrderResponse extends Base {
    data: PurchaseOrder;
}

export interface UpsertPOCustomFieldValueOptions extends BaseOptions {
    poCustomField: POCustomField;
    Created: UpsertPOCustomFieldValueCallback;
}

export type UpsertPOCustomFieldValueCallback = (poCustomField: POCustomField) => void;

export interface UpsertPOCustomFieldValueResponse extends Base {
    data: POCustomField;
}

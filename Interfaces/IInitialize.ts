/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseOptions} from './Base';
import {Base} from '../Objects/Base';

export interface InitializeOptions extends BaseOptions {
    domain: string;
    version: string;
    auth_string: string;
    OK: InitializeCallback;
}

export type InitializeCallback = (authString: string) => void;

export interface InitializeResponse extends Base {
    data: {
        auth_string: string;
    };
}

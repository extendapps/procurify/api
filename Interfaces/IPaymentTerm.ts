/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseOptions} from './Base';
import {PaymentTerm} from '../Objects/PaymentTerm';
import {Base} from '../Objects/Base';

export interface DeletePaymentTermOptions extends BaseOptions {
    id: number;
    Deleted: DeletePaymentTermCallback;
}

export interface CreatePaymentTermOptions extends BaseOptions {
    paymentTerm: PaymentTerm;
    Created: PaymentTermCallback;
}

export interface UpdatePaymentTermOptions extends BaseOptions {
    id: number;
    paymentTerm: PaymentTerm;
    Updated: PaymentTermCallback;
}

export type DeletePaymentTermCallback = () => void;

export type PaymentTermCallback = (paymentTerm: PaymentTerm) => void;

export interface PaymentTermResponse extends Base {
    data: PaymentTerm;
}

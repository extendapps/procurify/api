/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseOptions} from './Base';
import {ContentType, ContentTypeModel} from '../Objects/ContentType';
import {Base} from '../Objects/Base';

export interface GetContentTypesOptions extends BaseOptions {
    model?: ContentTypeModel;
    OK: GetContentTypesCallback;
}

export type GetContentTypesCallback = (contentTypes: ContentType[]) => void;

export interface GetContentTypesResponse extends Base {
    data: ContentType[];
}

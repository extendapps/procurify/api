/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseOptions, BaseStatusOptions} from './Base';
import {Vendor} from '../Objects/Vendor';
import {Base} from '../Objects/Base';

export interface DeleteVendorOptions extends BaseOptions {
    id: number;
    Deleted: DeleteVendorCallback;
}

export interface CreateVendorOptions extends BaseOptions {
    vendor: Vendor;
    Created: VendorCallback;
}

export interface UpdateVendorOptions extends BaseOptions {
    id: number;
    vendor: Vendor;
    Updated: VendorCallback;
}

export interface GetVendorOptions extends BaseOptions {
    id: number;
    OK: VendorCallback;

    NotFound(): void;
}

export interface GetVendorsOptions extends BaseStatusOptions {
    OK: VendorsCallback;
}

export type DeleteVendorCallback = () => void;

export type VendorCallback = (vendor: Vendor) => void;

export type VendorsCallback = (vendors: Vendor[]) => void;

export interface VendorResponse extends Base {
    data: Vendor;
}

export interface VendorsResponse extends Base {
    data: Vendor[];
}

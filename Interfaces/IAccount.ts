/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {Account} from '../Objects/Account';
import {BaseOptions, ProcurifyStatus} from './Base';
import {Base} from '../Objects/Base';

export interface DeleteAccountOptions extends BaseOptions {
    id: number;
    Deleted: DeleteAccountCallback;
}

export interface GetAccountOptions extends BaseOptions {
    id: number;
    OK: AccountCallback;

    NotFound(): void;
}

export interface GetAccountsOptions extends BaseOptions {
    codes?: string[];
    status?: ProcurifyStatus;
    OK: AccountsCallback;
}

export interface CreateAccountOptions extends BaseOptions {
    account: Account;
    Created: AccountCallback;
}

export interface UpdateAccountOptions extends BaseOptions {
    id: number;
    account: Account;
    Updated: AccountCallback;
}

export type DeleteAccountCallback = () => void;

export type AccountsCallback = (accounts: Account[]) => void;

export type AccountCallback = (account: Account) => void;

export interface AccountResponse extends Base {
    data: Account;
}

export interface AccountsResponse extends Base {
    data: Account[];
}

/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BadRequestCallBack, ServerCallBack} from '../procurifyapi';

export interface BaseOptions {
    BadRequest: BadRequestCallBack;
    Failed: ServerCallBack;
}

export interface BaseStatusOptions extends BaseOptions {
    status?: ProcurifyStatus;
    ids?: number[];
}

export type ProcurifyStatus = 'pending' | 'synced' | 'ignore';

/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {Base} from '../Objects/Base';
import {ObjectMap} from '../Objects/ObjectMap';
import {BaseOptions} from './Base';

export interface UpsertObjectMapOptions extends BaseOptions {
    objectMap: ObjectMap;
    Created: UpsertObjectMapCallback;
}

export interface BatchUpsertObjectMapOptions extends BaseOptions {
    objectMaps: ObjectMap[];
    Created: BatchUpsertObjectMapCallback;
}

export type UpsertObjectMapCallback = (objectMap: ObjectMap) => void;

export type BatchUpsertObjectMapCallback = (objectMaps: ObjectMap[]) => void;

export interface UpsertObjectMapResponse extends Base {
    data: ObjectMap;
}

export interface BatchUpsertObjectMapResponse extends Base {
    data: ObjectMap[];
}

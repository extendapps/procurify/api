/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {Base} from '../Objects/Base';
import {ItemReceiptLine} from '../Objects/ItemReceiptLine';
import {BaseStatusOptions} from './Base';

export interface GetItemReceiptLinesOptions extends BaseStatusOptions {
    OK: GetItemReceiptLinesCallback;
}

export type GetItemReceiptLinesCallback = (itemReceiptLines: ItemReceiptLine[]) => void;

export interface GetItemReceiptLinesResponse extends Base {
    data: ItemReceiptLine[];
}

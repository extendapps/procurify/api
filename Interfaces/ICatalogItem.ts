/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {BaseOptions} from './Base';
import {CatalogItem} from '../Objects/CatalogItem';
import {Base} from '../Objects/Base';

export interface DeleteCatalogItemOptions extends BaseOptions {
    id: number;
    Deleted: DeleteCatalogItemCallback;
}

export interface CreateCatalogItemOptions extends BaseOptions {
    catalogItem: CatalogItem;
    Created: CatalogItemCallback;
}

export interface UpdateCatalogItemOptions extends BaseOptions {
    id: number;
    catalogItem: CatalogItem;
    Updated: CatalogItemCallback;
}

export interface GetCatalogItemOptions extends BaseOptions {
    id: number;
    OK: CatalogItemCallback;

    NotFound(): void;
}

export interface GetCatalogItemsOptions extends BaseOptions {
    OK: CatalogItemsCallback;
}

export type DeleteCatalogItemCallback = () => void;

export type CatalogItemCallback = (catalogItem: CatalogItem) => void;

export type CatalogItemsCallback = (catalogItems: CatalogItem[]) => void;

export interface CatalogItemResponse extends Base {
    data: CatalogItem;
}

export interface CatalogItemsResponse extends Base {
    data: CatalogItem[];
}
